<table>
    <caption>DB Queries: <?= count($db['queries']) ?> / Time: <?= $db['total'] ?>ms</caption>
    <?php foreach($db['queries'] as $query): ?>
        <tr>
            <td><?= $query['query'] ?></td>
            <td style="text-align:right"><?= $query['time'] ?>ms</td>
        </tr>
    <?php endforeach ?>
</table>
