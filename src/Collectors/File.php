<?php namespace Pitekantrop\Profilar\Collectors;

class File implements CollectorInterface
{
	protected $files;

	/**
	 * @param array $files
	 */
	public function __construct(array $files = null)
	{
		$this->files = $files ?: get_included_files();
	}

	/**
	 * Get collector id
	 *
	 * @return string
	 */
	public function id()
	{
		return 'file';
	}

	/**
	 * Get an array of views to be included
	 *
	 * @return array
	 */
	public function views()
	{
		return [__DIR__.'/../../views/file.php'];
	}

	/**
	 * Collect data
	 *
	 * @return array
	 */
	public function collect()
	{
		return array_filter($this->files, function($path) 
		{
			return strpos($path, 'profilar') === false;
		});
	}
}