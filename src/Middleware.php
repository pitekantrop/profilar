<?php namespace Pitekantrop\Profilar;

use Closure;

class Middleware
{
	/**
	 * Handle an incoming request.
	 *
	 * @param  Illuminate\Http\Request  $request
	 * @param  Closure  $next
	 * @return mixed
	 */
	public function handle($request, Closure $next)
	{
		if ($this->shouldProfile($request))
		{
			$this->app = app();

			return $this->profile($request, $next);
		}

		return $next($request);
	}

	/**
	 * Determine if the request should be profiled
	 *
	 * @param  Illuminate\Http\Request $request
	 * @return boolean
	 */
	protected function shouldProfile($request)
	{
		return $request->query->has('profile');
	}

	/**
	 * Profile the request
	 *
	 * @param  Illuminate\Http\Request $request
	 * @param  Closure $next
	 * @return Symfony\Component\HttpFoundation\Response
	 */
	protected function profile($request, $next)
	{
		class_alias(Collectors\Timestamp::class, 'Profilar');

		$this->enableQueryLog();

		$next($request);

		$stop  = microtime(true);
		$start = defined('LARAVEL_START') ? LARAVEL_START : $request->server->get('REQUEST_TIME_FLOAT');
		$files = get_included_files();

		$profiler = new Profiler($this->app['view'], $request);

		$profiler->addCollector(new Collectors\Timestamp($start, $stop));
		$profiler->addCollector(new Collectors\Database($this->app['db']));
		$profiler->addCollector(new Collectors\File($files));
		$profiler->addCollector(new Collectors\Memory);

		return $profiler->getResponse();
	}


	/**
	 * Enable the database query log but only if the database
	 * binding has been resolved from the container
	 *
	 * @return void
	 */
	protected function enableQueryLog()
	{
		if ($this->app->resolved('db'))
		{
			$this->app['db']->enableQueryLog(); return;
		}

		$this->app->resolving('db', function($db)
		{
			$db->enableQueryLog();
		});
	}
}
