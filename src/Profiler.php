<?php namespace Pitekantrop\Profilar;

use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Contracts\View\Factory as ViewFactory;
use Pitekantrop\Profilar\Collectors\CollectorInterface;

class Profiler
{
	/**
	 * All registered collectors
	 *
	 * @var array
	 */
	protected $collectors = [];

	/**
	 * All views returned by collectors
	 *
	 * @var array
	 */
	protected $views = [];

	/**
	 * Data collected by collectors
	 *
	 * @var array
	 */
	protected $data  = [];

	/**
	 * @param Illuminate\Contracts\View\Factory $view
	 * @param Illuminate\Http\Request 			$request
	 */
	public function __construct(ViewFactory $view, Request $request)
	{
		$this->view = $view;
		$this->request = $request;
	}

	/**
	 * Register a collector to be executed
	 *
	 * @param Pitekantrop\Profilar\Collectors\CollectorInterface $collector
	 */
	public function addCollector(CollectorInterface $collector)
	{
		$this->collectors[] = $collector;
	}

	/**
	 * Replace the standard response with the profiler response
	 *
	 * @return  Symfony\Component\HttpFoundation\Response $res
	 */
	public function getResponse()
	{
		foreach ($this->collectors as $collector)
		{
			$this->data[$collector->id()] = $collector->collect();
			$this->views = array_merge($this->views, $collector->views());
		}

		$this->data['views'] = $this->views;
		$this->data['request'] = $this->request;

		$content = $this->view->file(realpath(__DIR__.'/../views/base.php'), $this->data);

		return new Response($content);
	}
}
