<html>
<head>
    <title></title>
    <style type="text/css">
        body {
            background-color: #EEE;
            font-family: Verdana;
        }
        table {
            width: 100%;
            background-color: #272727;
            color: #ddd;
            font-size: 12px;
            border-collapse: collapse;
            margin-top: 10px;
        }
        caption {
            color:#222;
            background-color: #ED591A;
            border: 1px solid rgba(0,0,0, 0.08);
            text-transform: uppercase;
            font-weight: bold;
            text-align: center;
            border-radius: 1px;
            padding: 5px;
        }
        tr:nth-child(odd) {
            background-color: rgba(255,255,255, 0.1);
        }
        td {
            padding: 6px;
        }
        a, a:hover, a:visited {
            color:black;
            text-decoration: none;
        }
        .wrapper {
            margin:0 auto;
            max-width: 960px;
        }
        .large {
            padding: 10px;
            font-size: 14px;
        }
        .mono {
            font-family: monospace;
        }
    </style>
</head>
<body>
    <div class="wrapper">
        <?php foreach ($views as $view) include $view; ?>
    </div>
</body>
</html>
