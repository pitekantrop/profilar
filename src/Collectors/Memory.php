<?php namespace Pitekantrop\Profilar\Collectors;

class Memory implements CollectorInterface
{
	/**
	 * Get collector id
	 *
	 * @return string
	 */
	public function id()
	{
		return 'memory';
	}

	/**
	 * Get an array of views to include
	 *
	 * @return array
	 */
	public function views()
	{
		return [];
	}

	/**
	 * Collect memory data
	 *
	 * @return integer
	 */
	public function collect()
	{
		return number_format(memory_get_usage() / 1048576, 2);
	}
}