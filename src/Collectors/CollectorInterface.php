<?php namespace Pitekantrop\Profilar\Collectors; 

interface CollectorInterface 
{
	public function collect();
	public function views();
	public function id();
}