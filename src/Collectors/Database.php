<?php namespace Pitekantrop\Profilar\Collectors;

use Illuminate\Database\ConnectionResolverInterface;

class Database implements CollectorInterface
{	
	/**
	 * @param Illuminate\Database\ConnectionResolverInterface $db
	 */
	public function __construct(ConnectionResolverInterface $db)
	{
		$this->db = $db;
	}
	
	/**
	 * Get id of the collector
	 *
	 * @return string
	 */
	public function id()
	{
		return 'db';
	}

	/**
	 * Get an array of views to be included
	 *
	 * @return array
	 */
	public function views()
	{
		return [__DIR__.'/../../views/database.php'];
	}

	/**
	 * Collect data
	 *
	 * @return array
	 */
	public function collect()
	{
		$db['total'] = 0;
		$db['queries'] = [];

		$queries = $this->db->getQueryLog();
		
		foreach ($queries as $i => $query) 
		{
			$bindings = $query['bindings'];
			$db['total'] += $query['time'];

			$db['queries'][] = [
				'time'  => $query['time'],
				'query' => preg_replace_callback('/\?/', function($matches) use(&$bindings) 
				{
					return array_shift($bindings);

				}, $query['query'])
			];
		}

		return $db;
	}
}