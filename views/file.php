<table>
    <caption>Included Files: <?= count($file) ?></caption>
    <?php foreach($file as $i => $file): ?>
        <tr>
            <td><?= $file ?></td>
            <td style="text-align:right"><?= sprintf('%03d', ++$i) ?></td>
        </tr>
    <?php endforeach ?>
</table>
