<table>
	<caption class="large">Time: <?= $time['global'] ?>ms / Memory: <?= $memory ?>MB / <a href="<?= $request->fullUrl() ?>">Refresh</a></caption>
	<?php foreach ($time['markers'] as $mark): ?>
		<tr>
			<td><?= $mark['name'] ?></td>
			<td style="text-align:right"><?= $mark['time'] ?>ms</td>
		</tr>
	<?php endforeach ?>
</table>
