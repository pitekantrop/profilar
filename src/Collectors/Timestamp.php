<?php namespace Pitekantrop\Profilar\Collectors;

class Timestamp implements CollectorInterface
{
	static $markers = [];

	/**
	 * @param integer $start Time from which to start measuring
	 * @param integer $end   Time when to stop global mesurement
	 */
	public function __construct($start, $end)
	{
		$this->start = $start;
		$this->end = $end;
	}

	/**
	 * Get collector id
	 *
	 * @return string
	 */
	public function id()
	{
		return 'time';
	}

	/**
	 * Get an array of views to include
	 *
	 * @return array
	 */
	public function views()
	{
		return [__DIR__.'/../../views/time.php'];
	}

	/**
	 * Collect data
	 *
	 * @return array
	 */
	public function collect()
	{
		return [
			'global' => $this->formatTime($this->end - $this->start),
			'markers' => $this->getMarkerPoints(),
		];
	}

	/**
	 * Set timing marker.
	 * 
	 * For markers that measure time from the beginning use:
	 * ```
	 * static::mark('SomeMarker')
	 * ```
	 * 
	 * And for markers that measure time between two points use:
	 * ```
	 * static::mark('SomeMarker')
	 * // Some Code
	 * static::mark('SomeMarkerEnd')
	 * ```
	 * 
	 * @param  string $mark
	 * @return void
	 */
	public static function mark($mark)
	{
		static::$markers[$mark] = microtime(true);
	}

	/**
	 * Prepare all marker points that are set by static::mark()
	 * 
	 * @return array
	 */
	protected function getMarkerPoints()
	{
		$markers = [];

		foreach (static::$markers as $mark => $time) 
		{
			$endMark = $mark.'End';

			if (array_key_exists($endMark, static::$markers)) 
			{
				$time = static::$markers[$endMark] - $time;
			}
			else
			{
				$mark = 'start -> '.$mark;
				$time = $time - $this->start;
			}

			if (!strpos($endMark, 'EndEnd')) 
			{
				$markers[] = array(
					'name' => $mark,
					'time' => $this->formatTime($time),
				);
			}
		}

		return $markers;
	}

	/**
	 * Friendly time formatting
	 *
	 * @param  integer $timestamp
	 * @param  integer $points
	 * @return integer
	 */
	public function formatTime($timestamp, $points = 2)
	{
		return number_format($timestamp * 1000, $points);
	}
}