<?php namespace Pitekantrop\Profilar;

use Illuminate\Support\ServiceProvider as BaseProvider;
use Illuminate\Foundation\AliasLoader;
use Illuminate\Contracts\Http\Kernel;

class ServiceProvider extends BaseProvider
{
	/**
	 * Indicates if loading of the provider is deferred.
	 *
	 * @var bool
	 */
	protected $defer = false;

	/**
	 * Register the service provider.
	 *
	 * @return void
	 */
	public function register()
	{
		$this->app[Kernel::class]->pushMiddleware(Middleware::class);
	}

	/**
	 * Additional files to be compiled
	 *
	 * @return array
	 */
	public static function compiles()
	{
		return [realpath(__FILE__)];
	}
}
