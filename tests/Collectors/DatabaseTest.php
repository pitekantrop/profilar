<?php namespace Pitekantrop\Profilar\Tests;

use Pitekantrop\Profilar\Collectors\Database;

class ClassName extends \PHPUnit_Framework_TestCase
{
	public function setUp()
	{
		$this->db = $this->getMock('Illuminate\Database\ConnectionResolverInterface', ['getQueryLog']);
		$this->collector = new Database($this->db);
	}

	public function testDdataIsFormattedCorrectly()
	{
		$this->db->method('getQueryLog')->willReturn($this->getFakeQueryLog());

		$col = $this->collector->collect();

		$this->assertEquals('1.2', $col['total']);
		$this->assertEquals('select * from fakes where id = 1 and occupation = prophet', $col['queries'][0]['query']);
	}

	protected function getFakeQueryLog()
	{
		$data['time']  = '1.2';
		$data['query'] = 'select * from fakes where id = ? and occupation = ?';
		$data['bindings'] = [1, 'prophet'];

		return [$data];
	}
}